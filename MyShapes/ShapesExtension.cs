﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyShapes.Extention
{
    /// <summary>
    /// Class extended triangle functional
    /// </summary>
    public static class TriangleExtension
    {
        /// <summary>
        /// Method indicating triangle is Rectangular
        /// </summary>
        /// <param name="tr">Exemple extended Triangle Class</param>
        /// <returns>Boolean value indicating triangle is Rectangular</returns>
        public static bool IsRectangular(this Triangle tr)
        {
            double a = tr.AB, b = tr.BC, c = tr.CA;
            return (a * a == (b * b + c * c)) || (b * b == (a * a + c * c)) || (c * c == (a * a + b * b));
        }
    }
}
