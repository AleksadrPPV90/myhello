﻿using System;

namespace MyShapes
{
    /// <summary>
    /// 2D Point structure
    /// </summary>
    public struct Point
    {
        /// <summary>
        /// X coordinate property
        /// </summary>
        /// <remarks>
        /// Default value = 0
        /// </remarks>
        public double X { get; }
        /// <summary>
        /// Y coordinate property
        /// </summary>
        /// <remarks>
        /// Default value = 0
        /// </remarks>
        public double Y { get; }
        public Point(double X, double Y):this() { this.X = X; this.Y = Y; }
        public static Point operator +(Point A,Point B)
        {
            return new Point(A.X + B.X, A.Y + B.Y);
        }
        public static Point operator -(Point A, Point B)
        {
            return new Point(A.X - B.X, A.Y - B.Y);
        }
        public override string ToString()
        {
            return $"Point(X:{this.X},Y:{this.Y})";
        }
        public override bool Equals(object obj)
        {
            if (obj is Point)
            {
                Point B = (Point)obj;
                return this == B;
            }
            return false;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public static bool operator !=(Point A, Point B)
        {
            return A.X != B.X || A.Y != B.Y;
        }
        public static bool operator ==(Point A, Point B)
        {
            return A.X == B.X && A.Y == B.Y;
        }
    }
    public struct Side
    {
        public Point A { get; set; }
        public Point B { get; set; }
        public Side(Point A,Point B): this() { this.A = A; this.B = B; }
        public double Length () => Side.Length(this.A, this.B);
        public static double Length(Point A, Point B) => Math.Sqrt(Math.Pow(A.X - B.X, 2) + Math.Pow(A.Y - B.Y, 2));
    }
    public interface IShapeCalculator
    {
        double Square();
    }
    public abstract class Shape: IShapeCalculator
    {
        public string Name { get; set; }
        public virtual double Square()
        {
            return -1;
        }
    }
    public class InvalidPointsToTriangleException : Exception
    {
        public InvalidPointsToTriangleException(string message) : base(message) { }
    }
    public class InvalidSidesToTriangleException : Exception
    {
        public InvalidSidesToTriangleException(string message) : base(message) { }
    }
    public class Triangle : Shape
    {
        private Point a;
        /// <summary>
        /// The first point of the triangle named A 
        /// </summary>
        public Point A {
            get => this.a;
            //set
            //{
            //    CheckPoints(value, B, C);
            //    this.a = value;
            //}
        }
        private Point b;
        /// <summary>
        /// The second point of the triangle named B 
        /// </summary>
        public Point B
        {
            get => this.b;
            //set
            //{
            //    CheckPoints(A, value, C);
            //    this.b = value;
            //}
        }
        private Point c;
        /// <summary>
        /// The third point of the triangle named C 
        /// </summary>
        public Point C
        {
            get => this.c;
            //set
            //{
            //    CheckPoints(A, B, value);
            //    this.c = value;
            //}
        }
        /// <summary>
        /// The side of the triangle located between point A and point B 
        /// </summary>
        public double AB
        {
            get => Side.Length(this.A,this.B);
        }
        /// <summary>
        /// The side of the triangle located between point B and point C 
        /// </summary>
        public double BC
        {
            get => Side.Length(this.B, this.C);
        }
        /// <summary>
        /// The side of the triangle located between point C and point A 
        /// </summary>
        public double CA
        {
            get => Side.Length(this.C, this.A);
        }
        public Triangle() { }
        public Triangle(Point A, Point B, Point C):this()
        {
            CheckPoints(A, B, C);
            this.a = A;
            this.b = B;
            this.c = C;
        }
        public Triangle(double a, double b, double c):this()
        {
            CheckSides(a, b, c);
            this.a = new Point(0, 0);
            this.b = new Point(a, 0);
            double Xc = (c * c - b * b + a * a) / (2 * a);
            double Yc = Math.Sqrt(c * c - Xc * Xc);
            this.c = new Point(Xc, Yc);
        }
        public override double Square()
        {
            double a = AB, b = BC, c = CA;
            double p = (a + b + c) / 2;
            return Math.Round(Math.Sqrt(p * (p - a) * (p - b) * (p - c)),5);
        }
        public static void CheckPoints (Point A, Point B, Point C)
        {
            if ((A == B) || (B == C) || (C == A))
                throw new InvalidPointsToTriangleException("In triangle all three points must be different");
            CheckSides(Side.Length(A, B), Side.Length(C, B), Side.Length(A, C));
        }
        public static void CheckSides (double a, double b, double c)
        {
            if (a >= (b + c) || b >= (a + c) || c >= (a + b))
                throw new InvalidSidesToTriangleException("In a triangle, one of the sides must not be longer than the sum of the lengths of the two other sides");

        }
        public override string ToString()
        {
            return $"Triangle({nameof(Name)}='{Name}',A={this.A},B={this.B},C={this.C})";
        }
    }
    public class Circle: Shape
    {
        public Point Center { get; set; } = new Point(0, 0);
        public double Radius { get; set; } = 1;
        public Circle() { }
        public Circle(double Radius):this()
        {
            this.Radius = Radius;
        }
        public Circle(Point Center, double Radius): this()
        {
            this.Center = Center;
            this.Radius = Radius;
        }
        public override double Square()
        {
            return Math.PI * Math.Pow(this.Radius, 2);
        }
        public override string ToString()
        {
            return $"Circle({nameof(Name)}='{Name}',Co={Center},R={Radius})";
        }
    }
}
