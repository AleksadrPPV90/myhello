﻿using System;
using MyShapes;
using Xunit;

namespace XUnitTestShapes
{
    public class CircleTest
    {
        [Fact]
        public void SimpleCircleSquare1()
        {
            // Arrange
            Circle crcl = new Circle(1);

            // Act
            double result = crcl.Square();

            // Assert
            Assert.Equal(Math.PI, result);
        }
        [Fact]
        public void SimpleCircleSquare2()
        {
            // Arrange
            int r = 5;
            Circle crcl = new Circle(r);

            // Act
            double result = crcl.Square();

            // Assert
            Assert.Equal(Math.PI*r*r, result);
        }
    }
}
