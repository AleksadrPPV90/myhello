﻿using System;
using MyShapes;
using MyShapes.Extention;
using Xunit;

namespace XUnitTestShapes
{
    public class RectangularTriangleTest
    {
        [Fact]
        public void RectangularTriangle1()
        {
            // Arrange
            Triangle tr = new Triangle(3,4,5);

            // Act
            bool isRectangular = tr.IsRectangular();

            // Assert
            Assert.True(isRectangular);
        }
        [Fact]
        public void RectangularTriangle2()
        {
            // Arrange
            double alfa = Math.PI / 2;
            Func<Point, Point> rotate = 
                (p) => 
                    new Point(p.X * Math.Cos(alfa) - p.Y * Math.Sin(alfa), p.X * Math.Sin(alfa) + p.Y * Math.Cos(alfa));
            Point A = rotate(new Point(2, 3));
            Point B = rotate(new Point(2, -1));
            Point C = rotate(new Point(5, -1));
            Triangle tr = new Triangle(A, B, C);

            // Act
            bool isRectangular = tr.IsRectangular();

            // Assert
            Assert.True(isRectangular);
        }
        [Fact]
        public void NotRectangularTriangle()
        {
            // Arrange
            Triangle tr = new Triangle(3,7,5);

            // Act
            bool isRectangular = tr.IsRectangular();

            // Assert
            Assert.False(isRectangular);
        }
    }
}
