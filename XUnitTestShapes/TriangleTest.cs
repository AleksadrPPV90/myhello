using System;
using MyShapes;
using Xunit;

namespace XUnitTestShapes
{
    public class TriangleTest
    {
        [Fact]
        public void ExceptionCreateTriangle1()
        {

            // Arrange
            double a = 3, b = 4, c = a + b + (new Random()).NextDouble();
            bool hasException = false;

            // Act
            try
            {
                Triangle tr = new Triangle(a, b, c);
            }
            catch (InvalidSidesToTriangleException e)
            {
                hasException = true;
            }

            // Assert
            Assert.True(hasException);
        }
        [Fact]
        public void ExceptionCreateTriangle2()
        {

            // Arrange
            double a = 3, b = 4, c = a + b;
            bool hasException = false;

            // Act
            try
            {
                Triangle tr = new Triangle(a, b, c);
            }
            catch (InvalidSidesToTriangleException e)
            {
                hasException = true;
            }

            // Assert
            Assert.True(hasException);
        }
        [Fact]
        public void ExceptionCreateTriangle3()
        {

            // Arrange
            Point A = new Point(1, 2),
                  B = new Point(2, 2),
                  C = new Point(1, 2);
            bool hasException = false;

            // Act
            try
            {
                Triangle tr = new Triangle(A, B, C);
            }
            catch (InvalidPointsToTriangleException e)
            {
                hasException = true;
            }

            // Assert
            Assert.True(hasException);
        }
        [Fact]
        public void ExceptionCreateTriangle4()
        {

            // Arrange
            Point A = new Point(0, 0),
                  B = new Point(3, 2),
                  C = new Point(4.5, 3);
            bool hasException = false;

            // Act
            try
            {
                Triangle tr = new Triangle(A, B, C);
            }
            catch (InvalidSidesToTriangleException e)
            {
                hasException = true;
            }

            // Assert
            Assert.True(hasException);
        }

        [Fact]
        public void SimpleTriangleSquare1()
        {
            // Arrange
            Triangle tr = new Triangle(3, 4, 5);

            // Act
            double result = tr.Square();

            // Assert
            Assert.Equal(6, result);
        }
        [Fact]
        public void SimpleTriangleSquare2()
        {
            // Arrange
            Triangle tr = new Triangle(new Point(0, 0), new Point(0, 1), new Point(1, 0));

            // Act
            double result = tr.Square();

            // Assert
            Assert.Equal(0.5, result);
        }
    }
}
