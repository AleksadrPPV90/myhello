﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using MyShapes;
using MyShapes.Extention;

namespace MyHello
{
    class Program
    {
        static void Main(string[] args)
        {
            IEnumerable<IShapeCalculator> Shapes = new List<IShapeCalculator>
            {
                new Triangle(2,3,4) { Name="TriangleByThreeSide" },
                new Triangle(3,4,5) { Name="TriangleRectangular" },
                new Triangle(new Point(1.5,6),new Point(1.5,-3.4),new Point(-1.5,4)) {Name = "TriangleByThreePoint"},
                new Circle(5) { Name = "CircleInZeroPoint"},
                new Circle(new Point (5,3),5) { Name = "CircleIn53Point"}
            };
            foreach(var shape in Shapes)
            {
                Console.WriteLine("----------------------------------------------");
                Console.WriteLine($"{shape} has square = {shape.Square()}");
                if (shape is Triangle)
                {
                    Console.WriteLine("This triangle is " + (((Triangle)shape).IsRectangular() ? "rectangular" : "not rectangular"));
                }
            }
        }
    }
}
