﻿CREATE VIEW [dbo].[ProductCategoryView]
	AS SELECT Pr.[Name] AS Product, Cat.[Name] AS Category
	FROM 
		[dbo].[Product] AS Pr 
			LEFT JOIN [dbo].[ProductCategory] AS PrCat ON Pr.[Id] = PrCat.[IdProduct]
			LEFT JOIN [dbo].[Category] AS Cat ON PrCat.[IdCategory] = Cat.[Id]
