﻿CREATE TABLE [dbo].[ProductCategory]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [IdProduct] INT NOT NULL, 
    [IdCategory] INT NOT NULL
)
